#!/usr/bin/env bash

set -Eeuo pipefail

function get-config() {
	local commit="${1:?}"
	local path="${2:?}"

	git config --blob "${commit}:.ae-sync" --get "$path"
}

function get-ae-branches() {
	local commit="${1:?}"
	git config --blob "${commit}:.ae-sync" --get-regexp --name-only '^.*\.path$' | awk -F . '{print $1}' | uniq
}


function commit-tree() {
	local branch="${1:?}"
	local commit_tree_hash="${2:?}"
	local src_commit="${3:?}"

	local commit_msg="$(git cat-file commit $src_commit | sed '1,/^$/d')"
	local author="$(git log --format=%an -1 $src_commit)"
	local email="$(git log --format=%ae -1 $src_commit)"
	local date="$(git log --format=%aI -1 $src_commit)"

	new_commit="$(
		GIT_AUTHOR_NAME="$author" GIT_AUTHOR_EMAIL="$email" GIT_AUTHOR_DATE="$date" \
		GIT_COMMITTER_NAME="$author" GIT_COMMITTER_EMAIL="$email" GIT_COMMITTER_DATE="$date" \
		git commit-tree -p "$branch" $commit_tree_hash -m "$commit_msg"
	)"

	git update-ref refs/heads/$branch $new_commit
	echo >&2 "ae-sync: Created new commit ${new_commit:0:7} on branch $branch ($(echo "$commit_msg" | head -n 1))"
}

function update-main-repo() {
	local branch=${1:?}

	# this should work if a Deploy key is set in the repo matching the SSH key injected into the CI environment
	# also ensure that the host key of $CI_SERVER_SHELL_SSH_HOST is added to SSH_KNOWN_HOSTS env var
	git push -f --quiet "git@$CI_SERVER_SHELL_SSH_HOST:$CI_PROJECT_PATH.git" "$branch"
}

function setup-ae-branch() {
	local ae_branch="${1:?}"
	local remote="${2:?}"
	local remote_branch="${3:?}"

	if ! git show-ref --quiet refs/heads/"$ae_branch"; then
		echo >&2 "ae-sync: Branch not available locally, fetching..."
		if ! git show-ref --quiet refs/remotes/origin/"$ae_branch"; then
			git fetch --quiet "$remote" "$remote_branch":"$ae_branch"
			update-main-repo "$ae_branch"
		else
			git fetch --quiet origin "$ae_branch":"$ae_branch"
		fi
	fi
}

# get the tree hash of the passed subdirectory with symlinks replaced by their targets
# TODO: for larger trees, this is very slow, because it invokes Git for each subdirectory;
#       to make this fast, we'd probably have to go back to Michal's script, run just one
#       Git invocation and process the result using a hashmap; ugh
function get-subdir-tree-hash() {
	local commit="${1:?}"
	local path="${2:?}"

	# to get the hash without resolving symlinks, the following line is enough
	#git ls-tree --object-only -d "$commit" "$path"

	git ls-tree "$commit" "$path"/ | while read -r line; do
		[[ "$line" =~ ^([0-9]{6})\ ([a-z]+)\ ([a-z0-9]+)[[:space:]](.*)$ ]]
		if [[ "${BASH_REMATCH[1]}" == "120000" ]]; then
			# mode 120000 = symlink
			local orig_hash="${BASH_REMATCH[3]}"
			local orig_path="${BASH_REMATCH[4]}"

			# read the symlink target
			local target_rel_path="$(git cat-file blob "$orig_hash")"

			# read the target object info
			local target="$(git ls-tree --full-name "$commit" "$orig_path/../$target_rel_path")"
			if [[ -z "$target" ]]; then
				echo >&2 "ERROR: Symlink at '$orig_path' is pointing to non-existent target: $orig_path/../$target_rel_path"
				# this is very violent, but `exit 1` does not work, as we're in a subshell
				kill $$
			fi

			# build the inlined object by combining metadata from the symlink target with the name of the symlink
			echo "${target%%$'\t'*}"$'\t'"${orig_path#"$path/"}"
		elif [[ "${BASH_REMATCH[2]}" == "tree" ]]; then
			# subdirectory, recurse
			local subdir_hash="$(get-subdir-tree-hash "$commit" "${BASH_REMATCH[4]}")"
			echo "${BASH_REMATCH[1]} ${BASH_REMATCH[2]} $subdir_hash"$'\t'"${BASH_REMATCH[4]#"$path/"}"
		else
			# not symlink, just reuse the entry with removed $path segment
			echo "${BASH_REMATCH[1]} ${BASH_REMATCH[2]} ${BASH_REMATCH[3]}"$'\t'"${BASH_REMATCH[4]#"$path/"}"
		fi
	done | git mktree
}

function apply-commit() {
	local ae_branch="${1:?}"
	local commit="${2:?}"
	local path="${3:?}"
	local remote="${4:?}"
	local remote_branch="${5:?}"

	# ensure $ae_branch exists
	setup-ae-branch "$ae_branch" "$remote" "$remote_branch"

	# get the tree hash of the AE subdirectory
	subtree_hash="$(get-subdir-tree-hash "$commit" "$path")"
	# get the root tree hash of the last commit in $ae_branch
	prev_hash="$(git rev-parse "$ae_branch^{tree}")"

	if [[ "$prev_hash" != "$subtree_hash" ]]; then
		echo >&2 "ae-sync: Subtree '$path' changed, updating branch..."
		commit-tree "$ae_branch" "$subtree_hash" "$commit"

		if ! git push --quiet "$remote" "$ae_branch":"$remote_branch"; then
			# overwrite $ae_branch with remote
			git fetch --quiet --force "$remote" "$remote_branch":"$ae_branch"
			# re-commit the tree
			commit-tree "$ae_branch" "$subtree_hash" "$commit"
			# try to push it again; if it fails again, just abort
			git push --quiet "$remote" "$ae_branch":"$remote_branch"
		fi
		update-main-repo "$ae_branch"
	fi
}


# originally, we iterated over all added commits, but that was slower and broke on force pushes
# (because CI then doesn't see the previous commit); now, only HEAD is used
# the downside is that it does not record commits when multiple commits are pushed at once, but the end result is correct
#
#commits=( $(git rev-list --reverse "${CI_COMMIT_BEFORE_SHA:?}".."${CI_COMMIT_SHA:?}") )
#for commit in "${commits[@]}"; do

echo >&2 "ae-sync: Checking for AE updates"

commit="HEAD"
# separate statement, because errors are ignored in a for loop generator
ae_branches=( $(get-ae-branches "$commit") )
for ae_branch in "${ae_branches[@]}"; do
	echo >&2 "ae-sync: Checking branch $ae_branch"
	path="$(get-config "$commit" "$ae_branch".path)"
	remote="$(get-config "$commit" "$ae_branch".url)"
	remote_branch="$(get-config "$commit" "$ae_branch".branch || echo "master")"
	
	apply-commit "$ae_branch" "$commit" "$path" "$remote" "$remote_branch"
done
