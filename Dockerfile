FROM alpine:latest
RUN apk add --no-cache git bash openssh-client
COPY --chmod=555 ./src/ae-sync.sh /usr/local/bin/ae-sync
