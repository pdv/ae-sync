# PDV AE Sync

Gitlab CI action, which uploads selected subdirectories from a main repository to configured Git remotes. We use it in PDV to synchronize BRUTE AE code from a single central repository to per-assignment repositories at https://brutegit.felk.cvut.cz/, from which the AE Docker images are generated.

Adapted from https://gitlab.fel.cvut.cz/sojkam1/brute-ae-sync. Main differences:

1) This script only runs in Gitlab CI.
2) Michal's script allows multiple paths for each remote, my version allows only one, but inlines any top-level symlinks leading out of the directory, which may be more convenient if you need more control over the directory layout.
3) This script does not have a separate first-time setup – if a new AE remote is added in the config file, it is automatically cloned on first CI run.

## Setup for Gitlab CI

1. Create an `.ae-sync` configuration file in the root of your repository and configure the synced AE repositories. The file uses the Git config file format, with the following structure:
   ```ini
   # name of an internal Git branch which ae-sync uses to locally track the history of the uploaded subdirectory
   [ae-01test]
   # path to the uploaded subdirectory
   path = ae/01test
   # URL of the target Git remote
   url = https://brutegit.felk.cvut.cz/course_org/01test
   
   [ae-02test]
   path = ae/02test
   url = https://brutegit.felk.cvut.cz/course_org/02test
   
   # ...
   ```
   
2. Include the CI configuration file in your `.gitlab-ci.yml` using one of the following options:
   ```yaml
   # 1) if your project is on gitlab.fel.cvut.cz
   include: {project: 'pdv/ae-sync', file: '/src/ae-sync.yml'}
   # 2) if your project is on a different Gitlab instance
   include: {remote: 'https://gitlab.fel.cvut.cz/pdv/ae-sync/-/blob/main/src/ae-sync.yml'}
   ```
3. Change `Git strategy` to `git clone` in `Settings -> CI/CD -> General pipelines`
4. the following CI variables must be set in `Settings -> CI/CD -> Variables`:
    - `SSH_KNOWN_HOSTS` = output of `ssh-keyscan brutegit.felk.cvut.cz gitlab.fel.cvut.cz`
    - `BRUTE_SSH_KEY` = SSH key, which is set as
    	1. Deploy Key for the Gitlab project, with write access (`Settings -> Repository -> Deploy keys`)
    	2. SSH key for one of the teachers on https://brutegit.felk.cvut.cz/profile/keys
